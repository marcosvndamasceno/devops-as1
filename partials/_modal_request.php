<div class="modal" id="modal-request">
    <div class="modal-content">
        <form action="request.php" method="POST">
            <div class="form-item">
                <b>Item:</b>
                <p class="item_name"></p>
            </div>
            <div class="form-item">
                <b>Emprestar para:</b>
                <p class="borrow_user"></p>
            </div>
            <div class="form-item">
                <input type="hidden" name="id">
                <input type="hidden" name="item_id">
                <label for="return_date">Data de devolução</label>
                <input type="date" name="return_date" id="return_date" required>
            </div>
            <div class="form-btn">
                <button type="button" data-modal="modal-request" onclick="closeModal($(this))"><span class="material-icons-outlined">
                        cancel
                    </span> Cancelar</button>
                <button type="submit"><span class="material-icons-outlined">
                        save
                    </span> Salvar</button>
            </div>
        </form>
    </div>
</div>