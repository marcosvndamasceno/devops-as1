<nav class="menu">
    <div class="menu-logo">
        <?php
        include('partials/_logo.php')
        ?>
        <h1>Me empresta!</h1>
    </div>
    <a href="#" class="hambuguer-menu" onclick="toggleMenu(event)">
        <span class="material-icons-outlined">
            menu
        </span>
    </a>
    <a href="/dashboard.php?content=items" class="nav-item">
        <span class="material-icons-outlined">
            inventory_2
        </span>
        <p>Meus Itens</p>
    </a>
    <a href="/dashboard.php?content=borrow" class="nav-item">
        <span class="material-icons-outlined">
            checklist
        </span>
        <p>Meus empréstimos</p>
    </a>
    <a href="/dashboard.php?content=search" class="nav-item">
        <span class="material-icons-outlined">
            manage_search
        </span>
        <p>Itens Disponíveis</p>
    </a>
    <a href="/dashboard.php?content=profile" class="nav-item">
        <span class="material-icons-outlined">
            account_circle
        </span>
        <p>Meu Perfil</p>
    </a>
    <?php

    if ($user['type']) {
        echo '<a href="/dashboard.php?content=users" class="nav-item">
        <span class="material-icons-outlined">
            people
        </span>
        <p>Usuários</p>
    </a><a href="/dashboard.php?content=all-items" class="nav-item">
        <span class="material-icons-outlined">
            format_list_bulleted
        </span>
        <p>Itens</p>
    </a>';
    }
    ?>
    <a href="logout.php" class="nav-item">
        <span class="material-icons-outlined">
            logout
        </span>
        <p>Sair</p>
    </a>
</nav>