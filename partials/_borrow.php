<?php

require('config.php');

session_start();
if ($_SESSION['user']) {
    $user = $_SESSION['user'];
    $sql = $pdo->prepare("SELECT borrows.*, items.name as item FROM borrows INNER JOIN items ON items.id = borrows.item_id WHERE borrow_user = :id");
    $sql->bindValue(':id', $user['id']);
    $sql->execute();
    $items = $sql->fetchAll();
} else {
    header("Location: index.php");
}
?>

<div class="content-title">
    <h4>Meus Empréstimos</h4>
</div>
<table id="items">
    <thead>
        <th>Código</th>
        <th>Item</th>
        <th>Status</th>
        <th>Devolução</th>
    </thead>
    <tbody>
        <?php
        if ($sql->rowCount() > 0) {
            foreach ($items as $item) {
                switch ($item['status']) {
                    case 1:
                        $status = 'Emprestado';
                        break;
                    case 2:
                        $status = 'Devolvido';
                        break;
                    default:
                        $status = 'Solicitado';
                        break;
                }
                if ($item['status'] == 2) {
                    $return_date = date('d/m/Y', strtotime($item['return_at']));
                } else {
                    if (!is_null($item['return_date'])) {
                        $return_date = date('d/m/Y', strtotime($item['return_date']));

                        if ($today > new DateTime($item['return_date'])) {
                            $return_date = '<p class="late">' . $return_date . '</p>';
                        }
                    } else {
                        $return_date = 'Sem data';
                    }
                }
                echo "
            <tr>
                <td>" . $item['id'] . "</td>
                <td>" . $item['item'] . "</td>
                <td>" . $status . "</td>
                <td style='display:flex;flex-direction:column;'>" . $return_date . "</td>
            </tr>
            ";
            }
        } else {
            echo '<tr>
            <td colspan="4">Você ainda não tem nenhum empréstimo!</td>
        </tr>';
        }
        ?>
    </tbody>
</table>