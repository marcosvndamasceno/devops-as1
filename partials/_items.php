<?php

require('config.php');

session_start();
if ($_SESSION['user']) {
    $user = $_SESSION['user'];
    $sql = $pdo->prepare("SELECT * FROM items WHERE user_id = :id");
    $sql->bindValue(':id', $user['id']);
    $sql->execute();
    $items = $sql->fetchAll();

    $sql = $pdo->prepare("SELECT borrows.*, users.name as user, users.email as email FROM borrows INNER JOIN users ON users.id = borrows.borrow_user WHERE owner_user = :id");
    $sql->bindValue(':id', $user['id']);
    $sql->execute();
    $requests = $sql->fetchAll();
} else {
    header("Location: index.php");
}
?>

<div class="content-title">
    <h4>Meus Itens</h4>
    <a href="#" class="new-item" onclick="openModal($(this));" data-modal="modal-item"><span class="material-icons-outlined">
            add_circle_outline
        </span> Adicionar Item</a>
</div>
<table id="items">
    <thead>
        <th>Código</th>
        <th>Item</th>
        <th>Status</th>
        <th>Ações</th>
    </thead>
    <tbody>
        <?php
        if (count($items) > 0) : ?>
            <?php foreach ($items as $item) : ?>
                <?php $item['status'] ? $status = 'Disponível' :
                    $status = 'Emprestado'; ?>
                <tr>
                    <td><?php echo $item['id']; ?></td>
                    <td><?php echo $item['name']; ?></td>
                    <td><?php echo $status; ?></td>
                    <td style="display:flex;
                    flex-direction:column;"><?php if (count($requests) > 0) : ?>
                            <?php foreach ($requests as $r) : ?>
                                <?php if ($r['item_id'] == $item['id']) : ?>
                                    <?php if ($r['status'] == 0 && $item['status'] == 1) : ?>
                                        <a href="#" onclick="event.preventDefault();modalRequest('<?php echo $r['id'] ?>', '<?php echo $r['user'] ?>', '<?php echo $item['name'] ?>', '<?php echo $item['id'] ?>', $(this))" data-modal="modal-request">Emprestar para: <?php echo $r['user'] ?></a>
                                    <?php elseif ($r['status'] == 1) : ?>
                                        Emprestado para <?php echo $r['user'] ?>. Contate-se via: <?php echo $r['email'] ?>
                                        <a href="request.php?giveback=<?php echo $r['id']; ?>">Marcar com devolvido</a>
                                    <?php endif ?>
                                <?php endif ?>
                            <?php endforeach ?>
                        <?php endif ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else : ?>
            <tr>
                <td colspan=" 4">Você ainda não adicionou nenhum item!
                </td>
            </tr>;
        <?php endif; ?>
    </tbody>
</table>