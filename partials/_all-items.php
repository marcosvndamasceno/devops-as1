<?php

require('config.php');

session_start();
if ($_SESSION['user']) {
    $user = $_SESSION['user'];
    $sql = $pdo->prepare("SELECT items.*, users.name as owner FROM items INNER JOIN users ON users.id = items.user_id");
    $sql->execute();
    $items = $sql->fetchAll();
} else {
    header("Location: index.php");
}
?>

<div class="content-title">
    <h4>Itens</h4>

</div>
<table id="items">
    <thead>
        <th>Código</th>
        <th>Item</th>
        <th>Proprietário</th>
        <th>Status</th>
    </thead>
    <tbody>
        <?php
        if ($sql->rowCount() > 0) {
            foreach ($items as $item) {
                $item['status'] ? $status = 'Disponível' :
                    $status = 'Emprestado';
                echo "
            <tr>
                <td>" . $item['id'] . "</td>
                <td>" . $item['name'] . "</td>
                <td>" . $item['owner'] . "</td>
                <td>" . $status . "</td>
            </tr>
            ";
            }
        } else {
            echo '<tr>
            <td colspan="4">Você ainda não adicionou nenhum item!</td>
        </tr>';
        }
        ?>
    </tbody>
</table>