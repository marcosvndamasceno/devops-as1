<?php

require('config.php');

session_start();
if ($_SESSION['user']) {
    $user = $_SESSION['user'];
} else {
    header("Location: index.php");
}
?>
<div class="content-title">
    <h4>Meu Perfil</h4>
</div>
<form class="login-form" action="user.php" method="POST">
    <div class="form-item">
        <label for="name"><span class="material-icons-outlined">
                account_box
            </span> Nome</label>
        <input name="name" id="name" placeholder="Nome" type="text" value="<?php echo $user['name']; ?>">
    </div>
    <div class="form-item">
        <label for="email"><span class="material-icons-outlined">
                email
            </span> E-mail</label>
        <input name="email" id="email" placeholder="E-mail" type="email" value="<?php echo $user['email'];?>">
    </div>
    <div class="form-item">
        <label for="password"><span class="material-icons-outlined">
                lock
            </span>Senha</label>
        <input name="password" id="password" placeholder="Senha" type="password">
    </div>
    <div class="form-btn">
        <button><span class="material-icons-outlined">
                save
            </span> Salvar</button>
    </div>
</form>
