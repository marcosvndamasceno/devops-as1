<?php

require('config.php');

session_start();
if ($_SESSION['user']) {
    $user = $_SESSION['user'];
    $sql = $pdo->prepare("SELECT * FROM users");
    $sql->execute();
    $users = $sql->fetchAll();
} else {
    header("Location: index.php");
}
?>

<div class="content-title">
    <h4>Usuários</h4>
</div>
<table id="items">
    <thead>
        <th>Código</th>
        <th>Nome</th>
        <th>Ações</th>
    </thead>
    <tbody>
        <?php
        if ($sql->rowCount() > 0) {
            foreach ($users as $item) {
                echo "
                <tr>
                    <td>" . $item['id'] . "</td>
                    <td>" . $item['name'] . "</td>
                    <td>...</td>
                </tr>
            ";
            }
        } else {
            echo '<tr>
            <td colspan="4">Você ainda não adicionou nenhum item!</td>
        </tr>';
        }
        ?>
    </tbody>
</table>