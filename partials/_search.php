<?php

require('config.php');

session_start();
if ($_SESSION['user']) {
    $user = $_SESSION['user'];
    $sql = $pdo->prepare("SELECT items.*, users.name as owner FROM items INNER JOIN users ON items.user_id = users.id WHERE items.user_id != :id AND items.status = 1 ");
    $sql->bindValue(':id', $user['id']);
    $sql->execute();
    $items = $sql->fetchAll();

    $sql = $pdo->prepare("SELECT * FROM borrows WHERE borrow_user = :id AND status < 2");
    $sql->bindValue(':id', $user['id']);
    $sql->execute();
    $borrows = $sql->fetchAll();
} else {
    header("Location: index.php");
}
?>

<div class="content-title">
    <h4>Itens Disponíveis</h4>
</div>
<table id="items">
    <thead>
        <tr>
            <th>Item</th>
            <th>Proprietário</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($items) > 0) : ?>
            <?php foreach ($items as $item) : ?>
                <tr>
                    <td><?php echo $item['name'] ?></td>
                    <td><?php echo $item['owner'] ?></td>
                    <td><a href="/request.php?id=<?php echo $item['id'] ?>" class="borrow-it" <?php
                                                                                                foreach ($borrows as $b) {
                                                                                                    if ($b['item_id'] == $item['id']) {
                                                                                                        echo 'style= display:none;';
                                                                                                    }
                                                                                                }
                                                                                                ?> data-id="<?php echo $item['id'] ?>"><span class='material-icons-outlined'>question_answer</span>
                            Pedir emprestado</a>
                        <?php foreach ($borrows as $b) : if ($b['item_id'] == $item['id']) : ?>
                                <a href="/request.php?id=<?php echo $item['id'] ?>&cancel=<?php echo $b['id'] ?>" class="borrow-it" data-id="<?php echo $item['id'] ?>"><span class='material-icons-outlined'>question_answer</span>
                                    Cancelar Pedido</a>
                        <?php endif;
                        endforeach ?>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td colspan=" 4">Nenhum item disponível para empréstimo!</td>
            </tr>
        <?php endif ?>
    </tbody>
</table>