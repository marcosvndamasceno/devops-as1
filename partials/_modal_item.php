<div class="modal" id="modal-item">
    <div class="modal-content">
        <form action="item.php" method="POST">
            <div class="form-item">
                <label for="name"><span class="material-icons-outlined">
                        create
                    </span> Nome</label>
                <input name="name" id="name" placeholder="Nome" type="text">
            </div>
            <div class="form-item">
                <label for="status"><span class="material-icons-outlined">
                        info
                    </span> Status</label>
                <select name="status">
                    <option value="0">Emprestado</option>

                    <option value="1" selected>Disponível</option>
                </select>
            </div>
            <div class="form-btn">
                <button type="button" data-modal="modal-item" onclick="closeModal($(this))"><span class="material-icons-outlined">
                        cancel
                    </span> Cancelar</button>
                <button type="submit"><span class="material-icons-outlined">
                        save
                    </span> Salvar</button>
            </div>
        </form>
    </div>
</div>