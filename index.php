<?php
session_start();
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Me empresta!</title>
    <link rel="stylesheet" href="src/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Exo:wght@100;200;400;900&family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
</head>

<body>
    <div class="container">
        <div class="left">
            <div class="logo">
                <?php
                include('partials/_logo.php')
                ?>
                <h1>Me empresta!</h1>
            </div>
        </div>
        <div class="right">
            <div class="login-box">
                <div class="login-title">
                    <p>Faça seu login para continuar.</p>
                </div>
                <form class="login-form" action="login.php" method="POST">
                    <input name="new" type="hidden" value="0">
                    <div class="form-item">
                        <label for="email"><span class="material-icons-outlined">
                                email
                            </span> E-mail</label>
                        <input name="email" id="email" placeholder="E-mail" type="email">
                    </div>
                    <div class="form-item">
                        <label for="password"><span class="material-icons-outlined">
                                lock
                            </span>Senha</label>
                        <input name="password" id="password" placeholder="Senha" type="password">
                    </div>
                    <div class="new">
                        <p> Ainda não tem uma conta?</p>
                        <a href="#" class="new-user-link">Cadastre-se</a>
                    </div>
                    <div class="form-btn">
                        <button><span class="material-icons-outlined">
                                login
                            </span> Login</button>
                    </div>
                </form>
            </div>
            <div class="new-user-box">
                <div class="login-title">
                    <p>Cadastro</p>
                </div>
                <form class="login-form" action="login.php" method="POST">
                    <input name="new" type="hidden" value="1">
                    <div class="form-item">
                        <label for="name"><span class="material-icons-outlined">
                                account_box
                            </span> Nome</label>
                        <input name="name" id="name" placeholder="Nome" type="text">
                    </div>
                    <div class="form-item">
                        <label for="email"><span class="material-icons-outlined">
                                email
                            </span> E-mail</label>
                        <input name="email" id="email" placeholder="E-mail" type="email">
                    </div>
                    <div class="form-item">
                        <label for="password"><span class="material-icons-outlined">
                                lock
                            </span>Senha</label>
                        <input name="password" id="password" placeholder="Senha" type="password">
                    </div>
                    <div class="new">
                        <p> Já possui uma conta?</p>
                        <a href="#" class="login-link">Login</a>
                    </div>
                    <div class="form-btn">
                        <button><span class="material-icons-outlined">
                                person_add
                            </span> Cadastrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="src/script.js"></script>
    <?php
    if ($_SESSION['error']) {
        echo '<script>alertify.error("' . $_SESSION['error'] . '");</script>';
    }
    ?>
</body>

</html>