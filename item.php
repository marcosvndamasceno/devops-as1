<?php

require('config.php');
session_start();

$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_ADD_SLASHES);
$status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_NUMBER_INT);
$user  = $_SESSION['user'];

if ($name && $status) {
    $sql = $pdo->prepare('INSERT INTO items (name, status, user_id) VALUES (:name, :status, :user_id)');
    $sql->bindValue(':name', $name);
    $sql->bindValue(':status', $status);
    $sql->bindValue(':user_id', $user['id']);
    $sql->execute();

    header("Location: dashboard.php?content=items");
    exit;
}
