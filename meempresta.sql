-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           8.0.27-0ubuntu0.20.04.1 - (Ubuntu)
-- OS do Servidor:               Linux
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Copiando estrutura para tabela meempresta.borrows
DROP TABLE IF EXISTS `borrows`;
CREATE TABLE IF NOT EXISTS `borrows` (
  `id` int NOT NULL AUTO_INCREMENT,
  `owner_user` int NOT NULL DEFAULT '0',
  `borrow_user` int NOT NULL DEFAULT '0',
  `status` int NOT NULL,
  `item_id` int DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `return_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_borrows_users` (`owner_user`),
  KEY `FK_borrows_users_2` (`borrow_user`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `FK_borrows_items` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  CONSTRAINT `FK_borrows_users` FOREIGN KEY (`owner_user`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_borrows_users_2` FOREIGN KEY (`borrow_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela meempresta.borrows: ~2 rows (aproximadamente)
DELETE FROM `borrows`;
/*!40000 ALTER TABLE `borrows` DISABLE KEYS */;
INSERT INTO `borrows` (`id`, `owner_user`, `borrow_user`, `status`, `item_id`, `return_date`, `return_at`) VALUES
	(1, 3, 1, 2, 2, '2021-11-30', '2021-11-28'),
	(6, 3, 1, 2, 3, '2021-12-10', '2021-11-28'),
	(8, 3, 5, 2, 2, '2021-12-10', '2021-11-28'),
	(9, 1, 3, 1, 1, '2021-11-22', NULL),
	(10, 5, 3, 1, 4, '2021-12-16', NULL);
/*!40000 ALTER TABLE `borrows` ENABLE KEYS */;

-- Copiando estrutura para tabela meempresta.items
DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `user_id` int NOT NULL DEFAULT '0',
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_items_users` (`user_id`),
  CONSTRAINT `FK_items_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela meempresta.items: ~2 rows (aproximadamente)
DELETE FROM `items`;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`id`, `name`, `user_id`, `status`) VALUES
	(1, 'Bicicleta', 1, 0),
	(2, 'Livro: As crônicas de Nárnia', 3, 0),
	(3, 'Jogo Assassin´s Creed Origins', 3, 1),
	(4, 'Skate', 5, 0);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Copiando estrutura para tabela meempresta.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL DEFAULT '0',
  `type` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;

-- Copiando dados para a tabela meempresta.users: ~3 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`) VALUES
	(1, 'Admin', 'admin@meempresta.com', '21232f297a57a5a743894a0e4a801fc3', 1),
	(3, 'Fulano', 'fulano@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 0),
	(5, 'Ciclano', 'ciclano@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
