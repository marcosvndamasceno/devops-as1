<?php
require('config.php');

$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
$pass = filter_input(INPUT_POST, 'password');
$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_ADD_SLASHES);
session_start();
$id = $_SESSION['user']['id'];
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if($name || $pass || $email){
	if(!empty($pass)){
		$sql = $pdo->prepare('UPDATE users SET password = :value WHERE id = :id');
		$sql->bindValue(':value',md5($pass));
		$sql->bindValue(':id',$id);
		$sql->execute();
	}
	if($name != $user['name']){
		$sql = $pdo->prepare('UPDATE users SET name = :value WHERE id = :id');
		$sql->bindValue(':value',$name);
		$sql->bindValue(':id',$id);
		$sql->execute();
	}
	if($email != $user['email']){
		$sql = $pdo->prepare('UPDATE users SET email = :value WHERE id = :id');
		$sql->bindValue(':value',$email);
		$sql->bindValue(':id',$id);
		$sql->execute();
	}
	$sql = $pdo->prepare('SELECT * FROM users WHERE id = :id');
	$sql->bindValue(':id',$id);
	$sql->execute();
    $user = $sql->fetch();
    $_SESSION['user'] = $user;
    header("Location: dashboard.php?content=profile");
    exit;
}