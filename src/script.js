$(jQuery).ready(function () {
    $(document).on('click', '.new-user-link', function (e) {
        e.preventDefault();
        console.log('click')
        $('.new-user-box').show('fast');
        $('.login-box').hide();

    });

    $(document).on('click', '.login-link', function (e) {
        e.preventDefault();
        $('.new-user-box').hide();
        $('.login-box').show('fast');

    });
    let menu = 0;
    window.toggleMenu = function toggleMenu(e) {
        e.preventDefault();
        if (menu == 0) {
            $('.menu a').css('justify-content', 'center');
            $('.menu').css('width', '60px');

            $('.menu p').hide();
            $('.menu-logo h1').hide();

            menu++;
        } else {
            menu = 0;
            $('.menu a').css('justify-content', 'flex-start');
            $('.menu').css('width', '15%');
            setTimeout(function () {
                $('.menu p').show();
                $('.menu-logo h1').show();
            }, 150)
        }
    };

    window.openModal = function openModal(element) {
        let target = $(element).data('modal');
        $('#' + target).css('display', 'flex');
    };

    window.closeModal = function closeModal(element) {
        let target = $(element).data('modal');
        $('#' + target).hide();
    };

    window.modalRequest = function (id, user, item, item_id, element) {
        $('#modal-request').find('.item_name').html(item);
        $('#modal-request').find('.borrow_user').html(user);
        $('#modal-request').find('input[name="id"]').val(id);
        $('#modal-request').find('input[name="item_id"]').val(item_id);
        openModal(element);
    }

});