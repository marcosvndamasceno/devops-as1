<?php
require('config.php');

$new = filter_input(INPUT_POST, 'new');
$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
$pass = filter_input(INPUT_POST, 'password');
session_start();
if ($new == 1 && $email && $pass) {
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_ADD_SLASHES);
    if ($name) {
        $check = $pdo->prepare("SELECT COUNT(id) as count FROM users WHERE email = :email");
        $check->bindValue(':email', $email);
        $check->execute();
        $check = $check->fetch();
        if ($check['count']  == 0) {
            $sql = $pdo->prepare('INSERT INTO users (name, email, password, type) VALUES (:name, :email, :password, 0)');
            $sql->bindValue(':name', $name);
            $sql->bindValue(':email', $email);
            $sql->bindValue(':password', md5($pass));
            $sql->execute();

            $sql = $pdo->prepare("SELECT * FROM users WHERE email = :email");
            $sql->bindValue(':email', $email);
            $sql->execute();
            $user = $sql->fetch();
            $_SESSION['user'] = $user;
        } else {
            $_SESSION['error'] = 'Email já cadastrado no sistema!';
            header("Location: index.php");
        }

        header("Location: dashboard.php?content=items");
        exit;
    }
} else {
    if ($email && $pass) {
        $sql = $pdo->prepare('SELECT * FROM users WHERE email = :email');
        $sql->bindValue(':email', $email);
        $sql->execute();

        $user = $sql->fetch();

        if ($user['password'] == md5($pass)) {
            $_SESSION['user'] = $user;
            header("Location: dashboard.php?content=items");
            exit;
        }
    }
    $_SESSION['error'] = 'Usuário e/ou senha inválidos!';
    header("Location: index.php");
    exit;
}
