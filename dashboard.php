<?php
require('config.php');
date_default_timezone_set('America/Sao_Paulo');
session_start();
$today = new DateTime("now");
if ($_SESSION['user']) {
    $id = $_SESSION['user']['id'];
    $sql = $pdo->prepare("SELECT * FROM users WHERE id = :id");
    $sql->bindValue(':id', $id);
    $sql->execute();
    $user = $sql->fetch();
    $content = filter_input(INPUT_GET, 'content');
    if ($content) {
        $_SESSION['content'] = $content;
        header("Location: dashboard.php");
    }
} else {
    header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Me empresta!</title>
    <link rel="stylesheet" href="src/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.2.0/dist/sweetalert2.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Exo:wght@100;200;400;900&family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body class="dashboard">
    <div class="container">
        <main>
            <?php
            include('partials/_menu.php')
            ?>
            <div class="content-box">
                <header class="content-header">
                    <h3>Olá, <?php echo $user['name']; ?></h3>
                </header>
                <section class="content">
                    <?php
                    if ($_SESSION['content']) {
                        include('partials/_' . $_SESSION['content'] . '.php');
                    }
                    ?>
                </section>
            </div>
        </main>
    </div>
    <?php
    include('partials/_modal_item.php');
    include('partials/_modal_request.php');
    ?>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.2.0/dist/sweetalert2.all.min.js"></script>
    <?php

    if (!is_null($_SESSION['success'])) {
        echo '<script>
                Swal.fire({
                    position: "top-end",
                    icon: "success",
                    title: "' . $_SESSION['success'] . '",
                    showConfirmButton: false,
                    timer: 1100
                    });
            </script>';
        $_SESSION['success'] = null;
    }
    ?>
    <script src="src/script.js"></script>
</body>

</html>