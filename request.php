<?php
require('config.php');

session_start();

if ($_SESSION['user']) {
    $id = $_SESSION['user']['id'];

    $sql = $pdo->prepare("SELECT * FROM users WHERE id = :id");
    $sql->bindValue(':id', $id);
    $sql->execute();

    $user = $sql->fetch();

    $item = filter_input(INPUT_GET, 'id');
    $cancel = filter_input(INPUT_GET, 'cancel');
    $giveback = filter_input(INPUT_GET, 'giveback');

    $hasPost = $_POST;

    $sql = $pdo->prepare("SELECT * FROM items WHERE id = :id");
    $sql->bindValue(':id', $item);
    $sql->execute();

    $item = $sql->fetch();

    if (count($hasPost) > 0) {
        $sql = $pdo->prepare("UPDATE borrows SET return_date = :return_date, status = 1 WHERE id = :id");
        $sql->bindValue(':id', $_POST['id']);
        $sql->bindValue(':return_date', $_POST['return_date']);
        $sql->execute();

        $sql = $pdo->prepare("UPDATE items SET  status = 0 WHERE id = :id");
        $sql->bindValue(':id', $_POST['item_id']);
        $sql->execute();

        $_SESSION['success'] = 'Solicitação aceita com sucesso!';
        $_SESSION['content'] = 'items';
    } else {
        if ($cancel > 0) {
            $sql = $pdo->prepare("DELETE FROM borrows WHERE id = :id");
            $sql->bindValue(':id', $cancel);
            $sql->execute();
            $_SESSION['success'] = 'Solicitação cancelada com sucesso!';
            $_SESSION['content'] = 'search';
        } else {
            if ($giveback > 0) {
                $sql = $pdo->prepare("UPDATE borrows SET status = 2, return_at = now() WHERE id = :id");
                $sql->bindValue(':id', $giveback);
                $sql->execute();
                $sql = $pdo->prepare("SELECT item_id FROM borrows WHERE id = :id");
                $sql->bindValue(':id', $giveback);
                $sql->execute();
                $item = $sql->fetch();
                $sql = $pdo->prepare("UPDATE items SET  status = 1 WHERE id = :id");
                $sql->bindValue(':id', $item['item_id']);
                $sql->execute();
                $_SESSION['success'] = 'Item devolvido com sucesso!';
                $_SESSION['content'] = 'items';
            } else {
                $sql = $pdo->prepare("INSERT INTO borrows (owner_user, borrow_user, status, item_id) VALUES ( :owner, :borrow, 0, :item)");
                $sql->bindValue(':owner', $item['user_id']);
                $sql->bindValue(':borrow', $id);
                $sql->bindValue(':item', $item['id']);
                $sql->execute();
                $_SESSION['success'] = 'Solicitação enviada com sucesso!';
                $_SESSION['content'] = 'search';
            }
        }
    }



    header("Location: dashboard.php");
} else {
    header("Location: index.php");
}
